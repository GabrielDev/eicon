#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /home/app
COPY pom.xml .
# RUN mvn -B -f pom.xml dependency:go-offline

COPY src ./src
RUN mvn -B install -Dmaven.test.skip=true

#
# Package stage
#
FROM java:8

ENV DATABASE_HOST ""
ENV DATABASE_PORT ""
ENV DATABASE_NAME ""
ENV DATABASE_USER ""
ENV DATABASE_PASSWORD ""

COPY ./database ./docker-entrypoint-initdb.d/
COPY --from=build /home/app/target/*.jar /usr/local/lib/eicon.jar

ENTRYPOINT ["java", "-jar", "/usr/local/lib/eicon.jar"]