package br.com.eicon.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eicon.model.Order;


@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

	Optional<Order> findOneByNumber(Integer number);
	
	@Query("SELECT o FROM Order o WHERE o.number = :number OR o.client.name LIKE %:client%")
	List<Order> findByCriteria(@Param("number") Integer number, @Param("client") String client);

	@Query("SELECT o FROM Order o WHERE o.createIn BETWEEN :start AND :end AND (o.number = :number OR o.client.name LIKE %:client%)")
	List<Order> findByCriteria(@Param("number") Integer number, @Param("client") String client, @Param("start") Date start, @Param("end") Date end);
}
