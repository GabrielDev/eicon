package br.com.eicon.controller;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.eicon.dto.form.ClientForm;
import br.com.eicon.dto.form.OrderForm;
import br.com.eicon.model.Client;
import br.com.eicon.model.Order;
import br.com.eicon.model.OrderItem;
import br.com.eicon.model.Product;
import br.com.eicon.repository.ClientRepository;
import br.com.eicon.repository.OrderItemRepository;
import br.com.eicon.repository.OrderRepository;
import br.com.eicon.repository.ProductRepository;

@RestController
@RequestMapping(value = "/order", 
produces = { "application/json", "application/xml" },
consumes = { "application/json", "application/xml" })
public class OrderController {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderItemRepository orderItemRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Order> getOne(@PathVariable Long id) {
		Optional<Order> order = orderRepository.findById(id);
		if(order.isPresent()) {
			return ResponseEntity.ok(order.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping()
	public List<Order> listAll(
			@RequestParam(required = false) Integer number,
			@RequestParam(required = false) String client,
			@RequestParam(required = false) 
			@DateTimeFormat(pattern = "dd-MM-yyyy") Date start,
			@RequestParam(required = false) 
			@DateTimeFormat(pattern = "dd-MM-yyyy") Date end
		) {
		
		if(number != null || start != null || !StringUtils.isEmpty(client)) {
			if(start != null) {
				end = (end == null)? setLastTime(start): setLastTime(end);
				return orderRepository.findByCriteria(number, client, start, end);
			}
			
			return orderRepository.findByCriteria(number, client);
		}
		
		return orderRepository.findAll();
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<Order> save(@RequestBody @Valid OrderForm form, UriComponentsBuilder uriBuilder) {
		if(form.isOrderNumberExists(orderRepository)) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		
		Order order = form.converter();
		Client client = this.saveClient(form.getClient());
		order.setClient(client);
		
		order = orderRepository.save(order);
		
		Set<OrderItem> itens = saveItens(order, form.getItens());
		order.setItens(itens);
		
		URI uri = uriBuilder.path("/order/{id}").buildAndExpand(order.getId()).toUri();
		return ResponseEntity.created(uri).body(order);
	}
	

	private Client saveClient(ClientForm form) {
		Client client = form.getClientIfExists(clientRepository);
		client = clientRepository.save(client);
		return client;
	}
	
	private Set<OrderItem> saveItens(Order order, List<OrderItem> itens) {
		for (OrderItem orderItem : itens) {
			Product product = saveProduct(orderItem.getProduct());
			orderItem.setProduct(product);
			orderItem.setOrder(order);
		}
		
		orderItemRepository.saveAll(itens);
		return new HashSet<OrderItem>(itens);
	}

	private Product saveProduct(Product product) {
		if(product.getDescription() != null) {
			Optional<Product> productExists = productRepository.findOneByDescription(product.getDescription());
			if(productExists.isPresent()) {
				product = productExists.get();
				return product;
			}
		}
		
		product = productRepository.saveAndFlush(product);
		return product;
	}
	
	private Date setLastTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, 23);
		c.add(Calendar.MINUTE, 59);
		c.add(Calendar.SECOND, 59);
		
		return c.getTime();
	}
	
	
	
}
