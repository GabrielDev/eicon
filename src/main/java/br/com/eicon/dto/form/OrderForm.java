package br.com.eicon.dto.form;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import br.com.eicon.model.Order;
import br.com.eicon.model.OrderItem;
import br.com.eicon.repository.OrderRepository;

public class OrderForm {
	
	private static Float DISCOUNT_FIVE_PERCENT = .05f;
	
	private static Float DISCOUNT_TEN_PERCENT = .1f;
	
	private Long id;
	
	@NotNull
	private Integer number;
	
	private ClientForm client;
	
	private Double discount = 0.0;
	
	private Double subtotal = 0.0;
	
	@NotNull
	private Double total = 0.0;
	
	@NotNull
	private List<OrderItem> itens;
	
	private Date createIn = new Date();

	public Order converter() {
		Order order = new Order(
				this.getId(),
				this.getNumber(),
				this.getDiscount(),
				this.getSubtotal(),
				this.getTotal(),
				this.getCreateIn());
		
		this.calculateTotal(order);
		
		return order;
	}
	
	public boolean isOrderNumberExists(OrderRepository repository) {
		Optional<Order> orderExists = repository.findOneByNumber(this.getNumber());
		return orderExists.isPresent();
	}
	
	private void calculateTotal(Order order) {
		Double subtotal = 0.0;
		Double discount = 0.0;
		
		for (OrderItem orderItem : this.getItens()) {
			calculateTotalItem(orderItem);
			subtotal += orderItem.getPriceSubtotal();
			discount += orderItem.getPriceDiscount();
		}
		
		Double total = subtotal - discount;
		
		order.setSubtotal(subtotal);
		order.setDiscount(discount);
		order.setTotal(total);
	}
	
	private void calculateTotalItem(OrderItem orderItem) {
		Double subtotal = orderItem.getProduct().getPrice() * orderItem.getQuantity();
		Double discount = applyDiscount(orderItem);
		Double total = Math.floor(subtotal - discount);
		
		orderItem.setPriceUnit(orderItem.getProduct().getPrice());
		orderItem.setPriceSubtotal(subtotal);
		orderItem.setPriceDiscount(discount);
		orderItem.setPriceTotal(total);
	}
	
	private Double applyDiscount(OrderItem item) {
		Integer quantity = item.getQuantity();
		Double discount = 0.0;
		
		if(quantity > 5) {
			discount = item.getProduct().getPrice() * DISCOUNT_FIVE_PERCENT;
		}
		
		if(quantity >= 10) {
			discount = item.getProduct().getPrice() * DISCOUNT_TEN_PERCENT;
		}
		
		return Math.floor(discount * quantity);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public ClientForm getClient() {
		return client;
	}

	public void setClient(ClientForm client) {
		this.client = client;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public List<OrderItem> getItens() {
		return itens;
	}

	public void setItens(List<OrderItem> itens) {
		this.itens = itens;
	}

	public Date getCreateIn() {
		return createIn;
	}

	public void setCreateIn(Date createIn) {
		this.createIn = createIn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderForm other = (OrderForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
