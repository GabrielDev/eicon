package br.com.eicon.dto.form;

import java.util.Optional;

import javax.validation.constraints.Size;

import br.com.eicon.model.Client;
import br.com.eicon.repository.ClientRepository;

public class ClientForm {
	
	private Long id;
	
	@Size(min = 3)
	@Size(max = 100)
	private String name;
	
	public Client converter() {
		return new Client(
				this.getId(),
				this.getName());
	}
	
	public Client getClientIfExists(ClientRepository repository) {
		Client client = this.converter();
		
		if(this.getId() != null) {
			Optional<Client> clientExists = repository.findById(this.getId());
			if(clientExists.isPresent()) {
				client = clientExists.get();
				return client;
			}
		}
		
		Optional<Client> clientExists = repository.findOneByName(this.getName());
		if(clientExists.isPresent()) {
			client = clientExists.get();
		}
		
		return client;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientForm other = (ClientForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
