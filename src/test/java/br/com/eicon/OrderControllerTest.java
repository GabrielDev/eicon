package br.com.eicon;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eicon.controller.OrderController;
import br.com.eicon.dto.form.ClientForm;
import br.com.eicon.dto.form.OrderForm;
import br.com.eicon.model.OrderItem;
import br.com.eicon.model.Product;
import br.com.eicon.repository.ClientRepository;
import br.com.eicon.repository.OrderItemRepository;
import br.com.eicon.repository.OrderRepository;
import br.com.eicon.repository.ProductRepository;
import br.com.eicon.utils.JsonUtils;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private OrderRepository orderRepository;
	
	@MockBean
	private OrderItemRepository orderItemRepository;
	
	@MockBean
	private ClientRepository clientRepository;
	
	@MockBean
	private ProductRepository productRepository;
	
	private Integer number = 1;

	@Test
	public void postOrderShouldSaveOrderAndCalculate() throws Exception {
		number = (int) (Math.random() * 1000 + 1);
		ClientForm client = new ClientForm();
		client.setName("Test Client");
		Product phone = new Product("Fone de Ouvido Bluetooth Edifier", 100d);
		List<OrderItem> itens = new ArrayList<OrderItem>();
		itens.add(new OrderItem(phone, 10));

		OrderForm order = new OrderForm();
		order.setNumber(number);
		order.setClient(client);
		order.setItens(itens);

		this.mockMvc.perform(post("/order")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(JsonUtils.toJson(order)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$number", is(order.getNumber())))
				.andExpect(jsonPath("$client.name", is("Test Client")))
				.andExpect(jsonPath("$itens[0].product.description", is(phone.getDescription())))
				.andExpect(jsonPath("$itens[0].priceTotal", is(phone.getPrice())))
				.andExpect(jsonPath("$itens[0].priceDiscount", is(100)))
				.andExpect(jsonPath("$subtotal", is(1000)))
				.andExpect(jsonPath("$discount", is(100)))
				.andExpect(jsonPath("$total", is(900)));
	}

	@Test
	public void getOrdersShouldReturnListOfOrders() throws Exception {
		this.mockMvc.perform(get("/order?number=" + number).contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].client.name", is("Test Client")));
	}

}
