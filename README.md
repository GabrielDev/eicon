# EICON - Avaliação

## Requisitos

Para executar essa aplicação instale as seguintes dependências:

- [Java](https://java.com/pt-BR/)

- [Maven](https://maven.apache.org/install.html)

- [Mysql](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/)

- [Docker](https://docs.docker.com/get-docker/) (opcional)

- [Insomnia](https://support.insomnia.rest/article/23-installation) (opcional)

## Instalação

A aplicação está preparada para ser executada localmente ou via Docker, conforme instruções abaixo.

### Localmente

Com MySQL instalado, crie uma nova database executando o script [data.sql](https://bitbucket.org/GabrielDev/eicon/src/master/database/data.sql). É possível alterar as configurações de conexão caso queira, mas lembre-se de exportar as variáveis de ambiente.

Execute as seguintes linhas de comando

Variáveis de ambiente

```
export DATABASE_HOST=localhost \
export DATABASE_PORT=3306 \
export DATABASE_NAME=eicon \
export DATABASE_USER=eicon \
export DATABASE_PASSWORD=senha
```

Instalação

`mvn clean install -Dmaven.test.skip=true`

Execução

`java -jar target/eicon-0.0.1-SNAPSHOT.jar `

### Docker

Caso tenha o Docker instalado, não será necessário instalar o MySQL e criar uma nova database, basta executar com a seguinte linha de comando

`docker-compose up -d --build`

## Como utilizar

É possível consumir a API com qualquer cliente REST (Insomnia, Postman, etc), ou via curl.
Um arquivo com todas as requisições está disponível, caso tenha o Insomnia instalado.

Todas as requisições estão preparadas para enviar e receber arquivos json ou xml.

### Criar novo pedido

Ao criar um pedido, um novo cliente e seus respectivos produtos serão criados caso não exista.
Os clientes e produtos poderão ser reutilizados em novos pedidos, caso seja informado o id, ou forem identificados pelo nome.

Todos os valores (subtotal, desconto e total) serão calculados baseado nas seguintes regras de desconto:

- Caso a quantidade seja maior que 5 será aplicado 5% de desconto no valor total

- Para quantidades a partir de 10 será aplicado 10% de desconto no valor total

O número do pedido deverá ser único, caso contrário será retornado o status _409 Conflict_

| Campo                     | Obrigatório | Padrão |
| :------------------------ | :---------- | :----- |
| number                    | sim         |        |
| client.id                 | não         |        |
| client.name               | sim         |        |
| itens.product.id          | não         |        |
| itens.product.description | sim         |        |
| itens.product.price       | sim         |        |
| itens.amount              | não         | 1      |

#### Requisição

```
curl --request POST \
  --url http://localhost:8080/order \
  --header 'accept: application/json' \
  --header 'content-type: application/json' \
  --data '{
	"number": 1,
	"client": {
		"name": "Gabriel dos Santos"
	},
	"itens": [
		{
			"product": {
				"description": "Monitor Gamer LED LG 34",
				"price": 3211.67
			},
			"quantity": 1
		},
		{
			"product": {
				"description": "Cadeira gamer",
				"price": 800
			},
			"quantity": 1
		},
		{
			"product": {
				"description": "Caneta",
				"price": 2
			},
			"quantity": 100
		}
	]
}'
```

#### Resposta

```
{
  "id": 1,
  "client": {
    "id": 1,
    "name": "Gabriel dos Santos"
  },
  "number": 1,
  "createIn": 1604520937132,
  "discount": 20.0,
  "subtotal": 4211.67,
  "total": 4191.67,
  "itens": [
    {
      "id": 1,
      "product": {
        "id": 1,
        "description": "Monitor Gamer LED LG 34",
        "price": 3211.67
      },
      "quantity": 1,
      "priceUnit": 3211.67,
      "priceDiscount": 0.0,
      "priceSubtotal": 3211.67,
      "priceTotal": 3211.67
    },
    {
      "id": 2,
      "product": {
        "id": 2,
        "description": "Cadeira gamer",
        "price": 800.0
      },
      "quantity": 1,
      "priceUnit": 800.0,
      "priceDiscount": 0.0,
      "priceSubtotal": 800.0,
      "priceTotal": 800.0
    },
    {
      "id": 3,
      "product": {
        "id": 3,
        "description": "Caneta",
        "price": 2.0
      },
      "quantity": 100,
      "priceUnit": 2.0,
      "priceDiscount": 20.0,
      "priceSubtotal": 200.0,
      "priceTotal": 180.0
    }
  ]
}
```

### Obter pedido

Opcionalmente é possível recuperar um pedido pelo ID

#### Requisição

```
curl --request GET \
  --url http://localhost:8080/order/1 \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

#### Resposta

```
{
  "id": 1,
  "client": {
    "id": 1,
    "name": "Gabriel dos Santos"
  },
  "number": 5,
  "createIn": 1604437387000,
  "discount": 22.0,
  "subtotal": 8249.0,
  "total": 8227.0,
  "itens": [
    {
      "id": 1,
      "product": {
        "id": 1,
        "description": "Notebook Lenovo Legion 5",
        "price": 8000.0
      },
      "quantity": 1,
      "priceUnit": 8000.0,
      "priceDiscount": 0.0,
      "priceSubtotal": 8000.0,
      "priceTotal": 8000.0
    },
    {
      "id": 2,
      "product": {
        "id": 2,
        "description": "Pilha AAA",
        "price": 7.0
      },
      "quantity": 7,
      "priceUnit": 7.0,
      "priceDiscount": 2.0,
      "priceSubtotal": 49.0,
      "priceTotal": 47.0
    },
    {
      "id": 3,
      "product": {
        "id": 3,
        "description": "Caneta",
        "price": 2.0
      },
      "quantity": 100,
      "priceUnit": 2.0,
      "priceDiscount": 20.0,
      "priceSubtotal": 200.0,
      "priceTotal": 180.0
    }
  ]
}
```

### Listar pedidos

É possível listar todos os pedidos ou fazer uma busca via _query string_ com os seguintes filtros

| Campo  | Tipo    | Exemplo    |
| :----- | :------ | :--------- |
| number | Integer | 1          |
| client | String  | Gabriel    |
| start  | Date    | 01-11-2020 |
| end    | Date    | 30-11-2020 |

Todos os filtros são opcionais, caso a data de fim (_end_) não seja informada, será filtrados todos os pedidos dentro de 24h a partir da data de início.

#### Requisição

```
curl --request GET \
  --url 'http://localhost:8080/order?start=01-11-2020&end=30-11-2020&client=Gabriel' \
  --header 'accept: application/json' \
  --header 'content-type: application/json'
```

#### Resposta

```
[{
  "id": 1,
  "client": {
    "id": 1,
    "name": "Gabriel dos Santos"
  },
  "number": 1,
  "createIn": 1604520937132,
  "discount": 20.0,
  "subtotal": 4211.67,
  "total": 4191.67,
  "itens": [
    {
      "id": 1,
      "product": {
        "id": 1,
        "description": "Monitor Gamer LED LG 34",
        "price": 3211.67
      },
      "quantity": 1,
      "priceUnit": 3211.67,
      "priceDiscount": 0.0,
      "priceSubtotal": 3211.67,
      "priceTotal": 3211.67
    },
    {
      "id": 2,
      "product": {
        "id": 2,
        "description": "Cadeira gamer",
        "price": 800.0
      },
      "quantity": 1,
      "priceUnit": 800.0,
      "priceDiscount": 0.0,
      "priceSubtotal": 800.0,
      "priceTotal": 800.0
    },
    {
      "id": 3,
      "product": {
        "id": 3,
        "description": "Caneta",
        "price": 2.0
      },
      "quantity": 100,
      "priceUnit": 2.0,
      "priceDiscount": 20.0,
      "priceSubtotal": 200.0,
      "priceTotal": 180.0
    }
  ]
},
{
  "id": 2,
  "client": {
    "id": 1,
    "name": "Gabriel dos Santos"
  },
  "number": 2,
  "createIn": 1604520937132,
  "discount": 20.0,
  "subtotal": 1000.0,
  "total": 980.0,
  "itens": [
    {
      "id": 2,
      "product": {
        "id": 2,
        "description": "Cadeira gamer",
        "price": 800.0
      },
      "quantity": 1,
      "priceUnit": 800.0,
      "priceDiscount": 0.0,
      "priceSubtotal": 800.0,
      "priceTotal": 800.0
    },
    {
      "id": 3,
      "product": {
        "id": 3,
        "description": "Caneta",
        "price": 2.0
      },
      "quantity": 100,
      "priceUnit": 2.0,
      "priceDiscount": 20.0,
      "priceSubtotal": 200.0,
      "priceTotal": 180.0
    }
  ]
}]
```

### Criar Pedido via XML

##### Requisição

```
curl --request POST \
  --url http://localhost:8080/order \
  --header 'accept: application/xml' \
  --header 'content-type: application/xml' \
  --data '<?xml version="1.0" encoding="UTF-8"?>
<Order>
  <client>
    <name>Gabriel dos Santos</name>
  </client>
  <number>1</number>
  <itens>
    <itens>
      <product>
        <description>Notebook Avell Liv 62</description>
        <price>6000.0</price>
      </product>
      <quantity>1</quantity>
    </itens>
    <itens>
      <product>
        <description>Pilha AAA</description>
        <price>7.0</price>
      </product>
      <quantity>7</quantity>
    </itens>
    <itens>
      <product>
        <description>Caneta</description>
        <price>2.0</price>
      </product>
      <quantity>100</quantity>
    </itens>
  </itens>
</Order>'
```

##### Resposta

```
<Order>
  <id>18</id>
  <client>
    <id>1</id>
    <name>Gabriel dos Santos</name>
  </client>
  <number>18</number>
  <createIn>1604521863208</createIn>
  <discount>22.0</discount>
  <subtotal>6249.0</subtotal>
  <total>6227.0</total>
  <itens>
    <itens>
      <id>36</id>
      <product>
        <id>8</id>
        <description>Notebook Avell Liv 62</description>
        <price>6000.0</price>
      </product>
      <quantity>1</quantity>
      <priceUnit>6000.0</priceUnit>
      <priceDiscount>0.0</priceDiscount>
      <priceSubtotal>6000.0</priceSubtotal>
      <priceTotal>6000.0</priceTotal>
    </itens>
    <itens>
      <id>37</id>
      <product>
        <id>2</id>
        <description>Pilha AAA</description>
        <price>7.0</price>
      </product>
      <quantity>7</quantity>
      <priceUnit>7.0</priceUnit>
      <priceDiscount>2.0</priceDiscount>
      <priceSubtotal>49.0</priceSubtotal>
      <priceTotal>47.0</priceTotal>
    </itens>
    <itens>
      <id>38</id>
      <product>
        <id>3</id>
        <description>Caneta</description>
        <price>2.0</price>
      </product>
      <quantity>100</quantity>
      <priceUnit>2.0</priceUnit>
      <priceDiscount>20.0</priceDiscount>
      <priceSubtotal>200.0</priceSubtotal>
      <priceTotal>180.0</priceTotal>
    </itens>
  </itens>
</Order>
```
